
fil = open('alternativ1.csv')
# Hopp over den første raden i filen
fil.readline()

forbruk = list()

# Les alle linjene i fila, én etter én
for linje in fil:
    # Del opp linjen ved hvert semikolon
    fra, til, kwh = linje.split(';') 

    # Del opp dato/klokkeslett ved mellomrom,
    # og hent de to første bokstavene i klokkeslettet (timer)
    dato = fra.split(' ')[0]
    kl_tekst = fra.split(' ')[1]
    kl = int(kl_tekst[:2])

    # Gjør om kwh-teksten til et tall
    kwh = float(kwh)

    data = (dato, kl, kwh)
    forbruk.append(data)

# Lukk filen og åpne den vi skal skrive til
fil.close()
rapport = open('rapport3.txt', 'w')

# Skriv ut antall datoer totalt
antall_datoer = 0
alle_datoer = list()
for data in forbruk:
    if data[0] not in alle_datoer:
        alle_datoer.append(data[0])
        antall_datoer += 1

rapport.write('Dager i måneden: ' + str(antall_datoer) + '\n')

# Skriv ut det høyeste forbruket
maks = 0
når_maks = None
for data in forbruk:
    if data[2] > maks:
        maks = data[2]
        når_maks = '{} kl {}'.format(data[0], data[1])

rapport.write('Maks forbruk på ' + når_maks)
rapport.write(', forbruk: ' + str(round(maks, 2)) + ' KWh.\n')

# Regn ut gjennomsnittet for alle klokkeslettene og skriv de ut
rapport.write('Gjennomsnitt per time:\n')
for time in range(24):
    totalt_forbruk = 0
    antall_forbruk = 0
    for data in forbruk:
        if data[1] == time:
            totalt_forbruk += data[2]
            antall_forbruk += 1
    average = totalt_forbruk / antall_forbruk
    rapport.write(' kl {}: {} KWh\n'.format(time, average))

rapport.close()
