
fil = open('alternativ1.csv')
fil.readline()

datoer = set()
klokkeslett = {}

for kl in range(24):
    klokkeslett[kl] = []

høyest_forbruk = 0
høyest_forbruk_tidspunkt = ''

for linje in fil:
    kolonne1, kolonne2, kolonne3 = linje.split(';') 

    dato, kl = kolonne1.split(' ')
    kl = int(kl[:2])

    kwh = float(kolonne3)

    if kwh > høyest_forbruk:
        høyest_forbruk = kwh
        høyest_forbruk_tidspunkt = kolonne1 + ' til ' + kolonne2
    datoer.add(dato)
    klokkeslett[kl].append(kwh)

fil.close()
rapport = open('rapport1.txt', 'w')

antall_datoer = len(datoer)
rapport.write(f'Antall dager i måneden: {antall_datoer}.\n')

rapport.write(f'Høyest forbruk var {høyest_forbruk_tidspunkt}, da var det {høyest_forbruk:.2f} KWh.\n')

rapport.write('Gjennomsnitt:\n')
for kl in range(24):
    forbruk = klokkeslett[kl]
    gjennomsnitt = sum(forbruk) / len(forbruk)
    rapport.write(f'  {kl}:00-{kl+1}:00 - {gjennomsnitt:.2f} KWh\n')

rapport.close()
