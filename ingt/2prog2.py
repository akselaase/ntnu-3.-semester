
fil = open('alternativ1.csv')
fil.readline()

datoer = list()

klokkeslett = {kl: list() for kl in range(24)}

maks_forbruk = 0
maks_forbruk_når = ''

for linje in fil:
    fra_tekst, til_tekst, kwh_tekst = linje.split(';') 

    dato, kl = fra_tekst.split(' ')
    kl = int(kl[:2])

    kwh = float(kwh_tekst)

    if kwh > maks_forbruk:
        maks_forbruk = kwh
        maks_forbruk_når = fra_tekst + ' til ' + til_tekst

    if dato not in datoer:
        datoer.append(dato)
    klokkeslett[kl].append(kwh)

fil.close()
rapport = open('rapport2.txt', 'w')

antall_datoer = len(datoer)
rapport.write('Antall dager i måneden: ')
rapport.write(str(antall_datoer))
rapport.write('\n')

rapport.write('Høyest forbruk i timen ')
rapport.write(maks_forbruk_når)
rapport.write(', forbruk: ')
rapport.write(str(round(maks_forbruk, 2)))
rapport.write(' KWh.\n')

rapport.write('Gjennomsnitt per time:\n')
for kl in range(24):
    forbruk_liste = klokkeslett[kl]
    gjennomsnitt = sum(forbruk_liste) / len(forbruk_liste)
    rapport.write('  ' + str(kl) + ':00 - ')
    rapport.write(str(round(gjennomsnitt, 2)) + ' KWh\n')

rapport.close()
