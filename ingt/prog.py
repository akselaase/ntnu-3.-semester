# Oppgave A

# Få en setning fra brukeren
tekst = input('Hei, skriv inn en setning: ')
# Del setningen i en liste med ord
ordene = tekst.split()
# Reverser listen, sånn at ordene er i motsatt rekkefølge
ordene.reverse()
# Kombiner de reverserte ordene til en ny setning
revers_setning = ' '.join(ordene)
# Skriv ut resultatet
print(revers_setning)


# Oppgave B
# Trenger funksjonen sqrt (square root) fra math-biblioteket
from math import sqrt

# Hent noen tall fra brukeren
starthastighet = float(input('Starthastiget: '))
lengde = float(input('Strekningens lengde: '))
akselerasjon = float(input('Akselerasjon: '))

# Bruk formelen og regn ut slutthastigheten
slutthastighet = sqrt(starthastighet ** 2 + 2 * akselerasjon * lengde)

# Sammenlikn slutthastigheten med det som sto på snl.no.
# Hvis den er for stor (139m/s=500km/h) setter vi en grense på 139
if slutthastighet > 139:
    slutthastighet = 139

print('Slutthastighet:', slutthastighet, 'm/s')


# Oppgave C

start_klokkeslett = 14
timer_senere = 535
# 535 // 24 gir 22, mens 525 / 24 gir 22.29. Trenger bare heltallsdelen her, ikke desimalene
dager_senere = timer_senere // 24
# % (modulo) betyr hvor mye som var igjen i rest etter å ha delt på 24. 535 / 24 gir 7 i rest,
# og 7 timer + 14 timer (startverdien) = 21 timer, så alarmen ringer kl 21
klokkeslett = (start_klokkeslett + timer_senere) % 24

print('Alarmen ringer', dager_senere, 'dager etter kl 14:00 i dag, og vil ringe kl', klokkeslett, 'på den dagen.')

