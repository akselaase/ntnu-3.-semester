import sys

from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

def laplace(t, f, S):
    n = len(t)
    dt = (np.max(t) - np.min(t)) / n
    res = np.zeros_like(S)
    for i in range(S.shape[0]):
        for j in range(S.shape[1]):
            res[i][j] = np.sum(f * np.exp(-S[i][j] * t)) * dt
    return res

def dirac(n):
    return np.array([1 if i == n//2 else 0 for i in range(n)])

def step(n):
    return np.array([1 if i >= n//2 else 0 for i in range(n)])

n = 100
X = np.linspace(0, 3, n)
Y = np.exp(2 * X) 
Sr = np.linspace(1, 3, n)
Si = np.linspace(1, 3, n)
Sr, Si = np.meshgrid(Sr, Si)
S = Sr + 1j * Si
L = laplace(X, Y, S)
Lmag = np.abs(L)
Larg = np.angle(L)

plt.plot(X, Y)
plt.show()

def plot3d(X, Y, Z, col):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    norm = colors.Normalize(col.min(), col.max())
    color = cm.jet(norm(col))

    surf = ax.plot_surface(X, Y, Z, facecolors=color, linewidth=0, antialiased=False)

    #ax.set_zlim(-1.01, 1.01)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    
    plt.xlabel('Re(s)')
    plt.ylabel('Im(s)')
#    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

plot3d(Sr, Si, Lmag, Larg)
