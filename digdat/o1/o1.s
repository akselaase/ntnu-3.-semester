.thumb
.syntax unified

.include "gpio_constants.s"     // Register-adresser og konstanter for GPIO

.text
	.global Start
	
ReadButton:

	ldr R0, =GPIO_BASE
	add R0, R0, #BUTTON_PORT * 9 * 4
	add R0, R0, #GPIO_PORT_DIN
	ldr R0, [R0]
	lsr R0, R0, #BUTTON_PIN
	eor R0, R0, #1
	and R0, R0, #1
	
	mov PC, LR

WriteLED:
	push {R1}

	lsl R0, R0, #LED_PIN
	ldr R1, =GPIO_BASE
	add R1, R1, #LED_PORT * 9 * 4
	add R1, R1, #GPIO_PORT_DOUT
	str R0, [R1]

	pop {R1}
	mov PC, LR

Task2:

	// R2 = LED_ADDR = GPIO_BASE + (9 * 4) * LED_PORT + GPIO_PORT_DOUTSET
	MOV R0, #LED_PORT
	MOV R1, #36
	MUL R2, R0, R1
	LDR R0, =GPIO_BASE
	MOV R1, #GPIO_PORT_DOUTSET
	ADD R2, R2, R0
	ADD R2, R2, R1
	// R0 = VALUE = 1 << LED_PIN
	MOV R1, #1
	LSL R0, R1, #LED_PIN
	// *LED_ADDR = VALUE
	STR R0, [R2]

	BX LR

Task3:
	PUSH {LR}
	BL ReadButton
	BL WriteLED
	POP {LR}
	B Task3

Start:
	PUSH {LR}
	BL Task3
	POP {LR}

NOP // Behold denne på bunnen av fila

