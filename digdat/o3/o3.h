/* En rask måte å unngå header recursion på er å sjekke om verdi, f.eks. 'O3_H',
   er definert. Hvis ikke, definer 'O3_H' og deretter innholdet av headeren 
   (merk endif på bunnen). Nå kan headeren inkluderes så mange ganger vi vil 
   uten at det blir noen problemer. */
#ifndef O3_H
#define O3_H

// Type-definisjoner fra std-bibliotekene
#include <stdint.h>
#include <stdbool.h>

// Type-aliaser
typedef uint32_t word;
typedef uint8_t  byte;

// Prototyper for bibliotekfunksjoner
void init(void);
void lcd_write(char* string);
void int_to_string(char *timestamp, unsigned int offset, int i);
void time_to_string(char *timestamp, int h, int m, int s);

// Prototyper
// legg prototyper for dine funksjoner her

#define S_SET_SECONDS	0
#define S_SET_MINUTES	1
#define S_SET_HOURS		2
#define S_COUNTDOWN		3
#define S_ALARM			4

extern int state;

int main();
void decrement_timer();
void update_lcd();
void set_state(int new_state);
void pb0_click();
void pb1_click();
void on_alarm();
void enable_gpio_interrupts();
void enable_systick();
void disable_systick();
void enable_led0();
void disable_led0();

#endif
