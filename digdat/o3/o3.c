#include "o3.h"
#include "gpio.h"
#include "systick.h"

volatile gpio_map_t* gpio = (gpio_map_t*) GPIO_BASE;
volatile systick_map_t* systick = (systick_map_t*) SYSTICK_BASE;

port_pin_t PB0 = { GPIO_PORT_B, 9 };
port_pin_t PB1 = { GPIO_PORT_B, 10 };
port_pin_t LED0 = { GPIO_PORT_E, 2 };

int state = S_SET_SECONDS;
int seconds = 0;

int main(void) {
    init();

	gpio->ports[LED0.port].DOUT = 0;
	gpio->ports[LED0.port].MODEL &= 0xFFFFF0FF;
	gpio->ports[LED0.port].MODEL |= GPIO_MODE_OUTPUT << 8;

	gpio->ports[PB0.port].DOUT = 0;
	gpio->ports[PB0.port].MODEH &= 0xFFFFF00F;
	gpio->ports[PB0.port].MODEH |= GPIO_MODE_INPUT << 8 | GPIO_MODE_INPUT << 4;
    
	enable_gpio_interrupts();
	update_lcd();

	while (true) {
		
	}

    return 0;
}

void pb0_click() {
	switch (state) {
		case S_SET_SECONDS:
			seconds++;
			break;

		case S_SET_MINUTES:
			seconds += 60;
			break;

		case S_SET_HOURS:
			seconds += 3600;
			break;
	}
	update_lcd();
}

void pb1_click() {
	switch (state) {
		case S_SET_SECONDS:
			set_state(S_SET_MINUTES);
			break;

		case S_SET_MINUTES:
			set_state(S_SET_HOURS);
			break;

		case S_SET_HOURS:
			set_state(S_COUNTDOWN);
			break;

		case S_ALARM:
			set_state(S_SET_SECONDS);
			break;
	}
}

void on_alarm() {
	set_state(S_ALARM);
}

void set_state(int new_state) {
	state = new_state;

	switch (new_state) {
		case S_SET_SECONDS: // from S_ALARM
			disable_led0();
			break;

		case S_SET_MINUTES:	// from S_SET_SECONDS
		case S_SET_HOURS:	// from S_SET_MINUTES
			break;

		case S_COUNTDOWN:	// from S_SET_HOURS
			enable_systick();
			break;

		case S_ALARM:		// from S_COUNTDOWN
			disable_systick();
			enable_led0();
			break;
	}
}

void enable_gpio_interrupts() {
	word port_sel = PB1.port << 8 | PB0.port << 4;
	word port_mask = 0xFFFFFF00;
	gpio->EXTIPSELH &= port_mask;
	gpio->EXTIPSELH |= port_sel;

	word buttons = 0;
	buttons |= 1 << PB0.pin;
	buttons |= 1 << PB1.pin;

	gpio->EXTIFALL |= buttons;
	gpio->IFC = buttons;
	gpio->IEN |= buttons;
}

void enable_systick() {
	systick->LOAD = FREQUENCY;
	systick->VAL = FREQUENCY;
	systick->CTRL = 0b111;
}

void disable_systick() {
	systick->CTRL = 0b110;
}

void enable_led0() {
	gpio->ports[LED0.port].DOUTSET = 1 << LED0.pin;
}

void disable_led0() {
	gpio->ports[LED0.port].DOUTCLR = 1 << LED0.pin;
}

void SysTick_Handler() {
	decrement_timer();
	update_lcd();
}

void decrement_timer() {
	if (--seconds == 0) {
		on_alarm();
	}
}

void update_lcd() {
	char timestamp[7];
	int h = seconds / 3600;
	int m = (seconds % 3600) / 60;
	int s = seconds % 60;
	time_to_string(timestamp, h, m, s);
	lcd_write(timestamp);
}

void GPIO_ODD_IRQHandler() {
	pb0_click();
	gpio->IFC |= 1 << PB0.pin;
}

void GPIO_EVEN_IRQHandler() {
	pb1_click();
	gpio->IFC |= 1 << PB1.pin;
}


/**************************************************************************//** * @brief Konverterer nummer til string 
 * Konverterer et nummer mellom 0 og 99 til string
 *****************************************************************************/
void int_to_string(char *timestamp, unsigned int offset, int i) {
    if (i > 99) {
        timestamp[offset]   = '9';
        timestamp[offset+1] = '9';
        return;
    }

    while (i > 0) {
	    if (i >= 10) {
		    i -= 10;
		    timestamp[offset]++;
		
	    } else {
		    timestamp[offset+1] = '0' + i;
		    i=0;
	    }
    }
}

/**************************************************************************//**
 * @brief Konverterer 3 tall til en timestamp-string
 * timestamp-argumentet må være et array med plass til (minst) 7 elementer.
 * Det kan deklareres i funksjonen som kaller som "char timestamp[7];"
 * Kallet blir dermed:
 * char timestamp[7];
 * time_to_string(timestamp, h, m, s);
 *****************************************************************************/
void time_to_string(char *timestamp, int h, int m, int s) {
    timestamp[0] = '0';
    timestamp[1] = '0';
    timestamp[2] = '0';
    timestamp[3] = '0';
    timestamp[4] = '0';
    timestamp[5] = '0';
    timestamp[6] = '\0';

    int_to_string(timestamp, 0, h);
    int_to_string(timestamp, 2, m);
    int_to_string(timestamp, 4, s);
}
