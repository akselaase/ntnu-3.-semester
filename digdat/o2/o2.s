.thumb
.syntax unified

.include "gpio_constants.s"     // Register-adresser og konstanter for GPIO
.include "sys-tick_constants.s" // Register-adresser og konstanter for SysTick

.text
	.global Start

Task1:
	PUSH {R0, R1}

	LDR R1, =tenths
	LDR R0, [R1]
	ADD R0, R0, #1
	STR R0, [R1]

	POP {R0, R1}
	BX LR

ToggleSysTick:
	// Sett opp sys-tick interrupt
	PUSH {R0, R1}

	LDR R0, =SYSTICK_BASE

	LDR R1, =(FREQUENCY/10)
	STR R1, [R0, #SYSTICK_LOAD]
	STR R1, [R0, #SYSTICK_VAL]

	LDR R1, [R0, #SYSTICK_CTRL]
	AND R1, R1, #0b001
	ORR R1, R1, #0b110
	EOR R1, R1, #0b001
	STR R1, [R0, #SYSTICK_CTRL]
	
	POP {R0, R1}
	BX LR

ToggleLed:
	// Toggle LED-en for hvert sekund
	PUSH {R0, R1}

	LDR R0, =GPIO_BASE
	MOV R1, #1
	LSL R1, R1, #LED_PIN
	STR R1, [R0, #(PORT_SIZE*LED_PORT+GPIO_PORT_DOUTTGL)]
	
	POP {R0, R1}
	BX LR

.global GPIO_ODD_IRQHandler
.thumb_func
GPIO_ODD_IRQHandler:
	PUSH {R0, R1}

	LDR R0, =GPIO_BASE

	PUSH {LR}
	BL ToggleSysTick
	POP {LR}

	MOV R1, #(1<<BUTTON_PIN)
	STR R1, [R0, #GPIO_IFC]

	POP {R0, R1}
	BX LR

.global SysTick_Handler
.thumb_func
SysTick_Handler:
	PUSH {R0, R1}

	LDR R1, =tenths
	LDR R0, [R1]
	ADD R0, R0, #1
	STR R0, [R1]

	CMP R0, #10
	BNE EndIf1
		MOV R0, #0
		STR R0, [R1]

		PUSH {LR}
		BL ToggleLed
		POP {LR}

		LDR R1, =seconds
		LDR R0, [R1]
		ADD R0, R0, #1
		STR R0, [R1]

		CMP R0, #60
		BNE EndIf2
			MOV R0, #0
			STR R0, [R1]

			LDR R1, =minutes
			LDR R0, [R1]
			ADD R0, R0, #1
			STR R0, [R1]
		EndIf2:
	EndIf1:

	POP {R0, R1}
	BX LR

Task7:
	// sett opp interrupt for knappen (seksjon 2.2.3)
	PUSH {R0, R1, R2}

	LDR R0, =GPIO_BASE
	MOV R2, #(1<<BUTTON_PIN)

	LDR R1, [R0, #GPIO_EXTIPSELH]
	BICS R1, R1, #0x00F0
	ORR R1, R1, #(PORT_B<<4)
	STR R1, [R0, #GPIO_EXTIPSELH]

	LDR R1, [R0, #GPIO_EXTIFALL]
	ORR R1, R1, R2
	STR R1, [R0, #GPIO_EXTIFALL]
	
	STR R2, [R0, #GPIO_IFC]

	LDR R1, [R0, #GPIO_IEN]
	ORR R1, R1, R2
	STR R1, [R0, #GPIO_IEN]
	
	POP {R0, R1, R2}
	BX LR
	
Start:

	PUSH {LR}
    BL Task7
	POP {LR}

	Loop:
		B Loop


NOP // Behold denne på bunnen av fila

