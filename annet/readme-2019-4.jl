
# generate sieve

sieve_length = 50000
primes = trues(sieve_length)
primes[1] = false
for n in 2:sieve_length
	last = sieve_length - sieve_length % n
	sub = view(primes, 2*n:n:last)
	fill!(sub, false)
end
primes = findall(primes)

function factors(n)
	res = []
	for p in primes
		if p > n
			break
		end
		while n % p == 0
			push!(res, p)
			n //= p
		end
	end
	return res
end

function calc()
	res1, res2, res3 = 0, 0, 0

	for n in 1:100000
		f = factors(n)
		s = Set(f)
		if length(f) == 2 
			res1 += n
		end
		if length(s) == 2
			res2 += n
		end
		if length(f) == length(s) == 2
			res3 += n
		end

		if n % 1000 == 0
			println(n, "/", 100000)
		end
	end

	return res1, res2, res3
end

res1, res2, res3 = calc()
println(res1)
println(res2)
println(res3)

# fac = factors(n)
# factors(187) = [11, 19]
# factors(4) = [2, 2]
# factors(20) = [2, 2, 5]
#
# med repeterte primtall: 1138479765
#  - kriterum: len(fac) == 2
#  - eks 4 = 2 * 2
#  - eks 10 = 2 * 5
#  - eks 187 = 11 * 19
#  - ikke 20 = 2 * 2 * 5
# med to distinkte primtall: 1617408111
#  - kriterium: len(set(fac)) == 2  
#  - eks 20 = 2 * 2 * 5
#  - eks 10 = 2 * 5
#  - eks 187 = 11 * 19
#  - ikke 4 = 2 * 2
# med kun unike, ikke-repeterende, primtall: 1136592401
#  - kriterium: len(fac) == len(set(fac)) == 2  
#  - eks 10 = 2 * 5
#  - eks 187 = 11 * 19
#  - ikke 4 = 2 * 2
#  - ikke 20 = 2 * 2 * 5
