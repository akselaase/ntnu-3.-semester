from .conditions import ConditionSet
from .rule import Rule


class RuleSet(Rule):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.conditions = ConditionSet(*args, **kwargs)
        self.rules = []

    def add_conditions(self, *args, **kwargs):
        self.conditions.extend(*args, **kwargs)

    def test_and_execute(self, state):
        if self.conditions.is_fulfilled(state):
            for rule in self.rules:
                if rule.test_and_execute(state):
                    return not self._passthrough
        return False

    def ruleset(self, *args, **kwargs):
        rules = RuleSet(*args, **kwargs)
        return self.add_rule(rules)

    def add_rule(self, rule=None):
        if rule is None:
            rule = Rule()
        self.rules.append(rule)
        return rule

    def when(self, *args, **kwargs):
        return self.add_rule().when(*args, **kwargs)

    def set_state(self, **kwargs):
        raise RuntimeError('set_state not supported on RuleSet')

    def call(self, *funcs):
        raise RuntimeError('call not supported on RuleSet')

    def __repr__(self):
        return f'RuleSet[{repr(self.conditions)}, {repr(self.rules)}]'
