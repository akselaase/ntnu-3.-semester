
fsm = FSM()

state = fsm.variable(name = 'state', initial = 'init')
signal = fsm.get_variable('signal')

init = fsm.when(state == 'init')
travel = fsm.when(state == 'travel')
exploration = fsm.when(state == 'exploration')
action = fsm.when(state == 'action')
land = fsm.when(state == 'land')

# state INIT
r_init_liftoff = init.when(signal == 'liftoff')
r_init_liftoff.set(state).to('travel')

# state TRAVEL
r_travel_waypoint = travel.when(signal == 'waypoint_reached')
r_travel_waypoint.call(calc_next_waypoint)

r_target_reached = travel.when(signal == 'arrived_at_mast')
r_target_reached.set(state).to('exploration')

r_home_reached = travel.when(signal == 'arrived_home')
r_home_reached.set(state).to('land')

# state EXPLORATION
r_explore_complete = exploration.when(signal == 'arrived_at_mast')
r_explore_complete.set(state).to('action')

# state ACTION
r_action_complete = action.when(signal == 'action_complete')
r_action_complete.set(state).to('travel')

# state LAND
r_land_complete = land.when(signal == 'landed')
r_land_complete.set(state).to('init')

