from .conditions import ConditionSet


class Rule:

    def __init__(self):
        self.conditions = ConditionSet()
        self.actions = []
        self._passthrough = False

    def test_and_execute(self, state):
        if self.conditions.is_fulfilled(state):
            for func in self.actions:
                func(state)
            return not self._passthrough
        return False

    def when(self, *args, **kwargs):
        self.conditions.extend(*args, **kwargs)
        return self

    def set_state(self, **kwargs):
        def apply_state(state):
            state.update(kwargs)
        apply_state._apply_state = kwargs #pylint: disable=protected-access
        return self.call(apply_state)

    def call(self, *funcs):
        self.actions.extend(funcs)
        return self

    def passthrough(self, passthrough=True):
        self._passthrough = passthrough

    def __str__(self):
        conditions = '\n'.join(map('   {0}'.format, self.conditions))
        if not self.conditions:
            conditions = '   None'
        actions = '\n'.join(map('   {0.__name__}'.format, self.actions))
        if not self.actions:
            actions = '   None'
        return f'Conditions:\n{conditions}\nActions:\n{actions}'

    def __repr__(self):
        if self.actions:
            def format_func(func):
                if hasattr(func, '_apply_state'):
                    return repr(func._apply_state)  # pylint: disable=protected-access
                return func.__name__
            text = ', '.join(map(format_func, self.actions))
            if len(self.actions) >= 2:
                text = f'[{text}]'

        else:
            text = 'no action'

        return f'{repr(self.conditions)} -> {text}'
