
class State:
    pass

class Variable:
    pass

fsm = FSM()

signal = fsm.variable('signal')
modus = fsm.variable('modus')

class INIT(State):

    def __init__(self):
        pass

    def on_enter(self):
        print('INIT enter')

    def on_exit(self):
        print('INIT exit')

    @signal.eq('liftoff')
    def on_liftoff(self):
        fsm.change_state(
