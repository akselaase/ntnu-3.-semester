class ConditionSet:

    def __init__(self, *args, **kwargs):
        self.conditions = []
        self.extend(*args, **kwargs)

    def extend(self, *conditions, **key_values):
        for func in conditions:
            if callable(func):
                self.conditions.append(func)
            else:
                raise ValueError(f'{func} is not a function')

        for key, value in key_values.items():
            key_condition = self._make_condition(key, value)
            self.conditions.append(key_condition)

    def is_fulfilled(self, state):
        def test_condition(cond):
            return cond(state)

        results = map(test_condition, self.conditions)
        return all(results)

    def __iter__(self):
        return iter(self.conditions)

    def __repr__(self):
        def format_func(func):
            if hasattr(func, '_condition_repr'):
                return func._condition_repr  # pylint: disable=protected-access
            else:
                return func.__name__
        funcs = ', '.join(map(format_func, self.conditions))
        return f"{{{funcs}}}"

    @staticmethod
    def _make_condition(key, value):
        if callable(value):
            def key_condition(state):
                return value(state[key])
            key_condition._condition_repr = f"{value.__name__}(state['{key}'])"  # pylint: disable=protected-access
        else:
            def key_condition(state):
                return state.get(key) == value
            key_condition._condition_repr = f'{repr(key)}: {repr(value)}'  # pylint: disable=protected-access
        return key_condition
