from .rules import RuleSet


class FSM:

    def __init__(self, **init_state):
        self.rules = RuleSet()
        self.state = init_state
        if 'signal' not in self.state:
            self.state['signal'] = None

    def ruleset(self, *args, **kwargs):
        return self.rules.ruleset(*args, **kwargs)

    def add_rule(self, rule=None):
        return self.rules.add_rule(rule)

    def when(self, *args, **kwargs):
        return self.add_rule().when(*args, **kwargs)

    def tick(self, signal):
        old_signal = self.state['signal']
        self.state['signal'] = signal

        if not self.rules.test_and_execute(self.state):
            self.state['signal'] = old_signal
            raise ValueError(
                f'No rule for current state ({self.state}) and signal ({signal}).')

    def __repr__(self):
        state = repr(self.state)
        rules = repr(self.rules.rules)
        return f'FSM(state={state}, rules={rules})'
