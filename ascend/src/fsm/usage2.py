# Not good

fsm = FSM()

signal = fsm.get_variable('signal')
state = fsm.variable(name = 'state', initial = 'init')
travel_direction = fsm.variable(name = 'travel_direction', initial = 'away')
travel_laps = fsm.variable(name = 'travel_laps', initial = 0)

INIT = fsm.state(state == 'init')
TRAVEL = fsm.state(state == 'travel')
TRAVEL_AWAY = TRAVEL.state(travel_direction == 'away')
TRAVEL_HOME = TRAVEL.state(travel_direction == 'home')
EXPLORATION = fsm.state(state == 'exploration')
ACTION = fsm.state(state == 'action')
LAND = fsm.state(state == 'land')

# state INIT
r_init_liftoff = INIT.when(signal == 'liftoff')
r_init_liftoff.set_state(TRAVEL_AWAY)

# state TRAVEL
TRAVEL.on_enter().set(travel_laps, 8)

r_lap_complete = TRAVEL.when(signal == 'waypoint_reached')
r_lap_complete.set(travel_laps, travel_laps - 1)

r_target_reached = r_lap_complete.when(travel_laps == 0)
r_target_reached.set_state(EXPLORATION)

r_lap_complete.call(calc_next_waypoint)

r_home_reached = TRAVEL_HOME.when(signal == 'waypoint_reached')
r_home_reached.set_state(LAND)

# state EXPLORATION
r_explore_complete = EXPLORATION.when(signal == 'arrived_at_mast')
r_explore_complete.set_state(ACTION)

# state ACTION
r_action_complete = ACTION.when(signal == 'action_complete')
r_action_complete.set_state(TRAVEL_HOME)

# state LAND
r_land_complete = LAND.when(signal == 'landed')
r_land_complete.set_state(INIT)

