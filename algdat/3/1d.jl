
function merge(L, R)
	if length(R) > length(L)
		L, R = R, L
	end
	i = 1
	j = 1
	res = []
	while i <= length(L)
		if (j > length(R) || L[i] < R[j])
			elem = L[i]
			i += 1
		else
			elem = R[j]
			j += 1
		end
		push!(res, elem)
	end
	while j <= length(R)
		push!(res, R[j])
		j += 1
	end
	return res
end

function mergesort(A)
	n = length(A)
	if n < 2
		return A
	end
	middle = (n + 1) >> 1
	L = A[1:middle]
	R = A[middle + 1:n]
	L = mergesort(L)
	R = mergesort(R)
	return merge(L, R)
end

a = [1, 3, 5]
b = [0, 2, 4]
