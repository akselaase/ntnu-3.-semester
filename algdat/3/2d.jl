
function mergearrays(L, R, axis)
	lenL = size(L, 1)
	lenR = size(R, 1)
	lenK = lenL + lenR
	if lenR > lenL
		L, R = R, L
		lenL, lenR = lenR, lenL
	end

	i, j, k = 1, 1, 1
	res = similar(L, (lenK, 2))

	while k <= lenK
		if i > lenL
			elem = R[j,:]
			j += 1
		elseif j > lenR
			elem = L[i,:]
			i += 1
		elseif L[i,axis] <= R[j,axis]
			elem = L[i,:]
			i += 1
		else
			elem = R[j,:]
			j += 1
		end
		res[k,:] = elem
		k += 1
	end

	return res
end

function mergesort(A, coord)
	n = size(A, 1)
	middle = (n + 1) >> 1
	if n < 2
		return A
	end

	L = A[1:middle,:]
	R = A[middle + 1:n,:]

	L = mergesort(L, coord)
	R = mergesort(R, coord)
	return mergearrays(L, R, coord)

end
a = [0 0; 0 1; 1 1; 1 2; 2 3; 2 4]
b = [1 0; 1 2; 2 2; 3 2; 3 3; 3 4]
c = vcat(a, b)
a1 = mergesort(c, 1)
a2 = mergesort(c, 2)

function find_median(A, coord)
	half_index = size(A, 1) >> 1 + 1
	if size(A, 1)% 2 == 0
		median = 0.5 * (A[half_index - 1, coord] + A[half_index, coord])
	else
		median = A[half_index, coord]
	end
	return median, half_index
end


function binaryintervalsearch(A, delta, coord)
	median, middle = find_median(A, coord)
	start, stop = middle, middle - 1

	while start > 1 && A[start - 1, coord] >= median - delta
		start -= 1
	end
	while stop < size(A, 1) && A[stop + 1, coord] <= median + delta
		stop += 1
	end
	
	if start > stop
		return -1, -1
	else
		return start, stop
	end
end

function sqrdist(p1, p2)
	return (p1[1] - p2[1]) ^ 2 + (p1[2] - p2[2]) ^ 2
end

function bruteforce(A)
	maxdist = Inf
	for i1 in 1:size(A, 1)
		p1 = A[i1, :]
		for i2 in (i1+1):size(A, 1)
			p2 = A[i2, :]
			dist = sqrdist(p1, p2)
			if dist < maxdist
				maxdist = dist
			end
		end
	end
	return sqrt(maxdist)
end

function splitintwo(x, y)
	x_middle = (size(x, 1) - 1) >> 1 + 1
	x_left = x[1:x_middle, :]
	x_right = x[x_middle + 1:end, :]

	y_left = similar(x_left)
	y_right = similar(x_right)
	left_i, right_i = 1, 1

	left_dict = Dict()
	right_dict = Dict()
	for i in 1:size(x_left, 1)
		p = x_left[i, :]
		left_dict[p] = 1 + get(left_dict, p, 0)
	end
	for i in 1:size(x_right, 1)
		p = x_right[i, :]
		right_dict[p] = 1 + get(right_dict, p, 0)
	end

	for i in 1:size(y, 1)
		p = y[i, :]
		if get(left_dict, p, 0) > 0
			y_left[left_i, :] = p
			left_i += 1
			left_dict[p] -= 1
		elseif get(right_dict, p, 0) > 0
			y_right[right_i, :] = p
			right_i += 1
			right_dict[p] -= 1
		else
			println("Oh shit")
		end
	end

	return x_left, x_right, y_left, y_right
end

points_scanned = Dict()

function closestpair(x, y)

	if size(x, 1) <= 3
		return bruteforce(x)
	end

	x_left, x_right, y_left, y_right = splitintwo(x, y)

	min_left = closestpair(x_left, y_left)
	min_right = closestpair(x_right, y_right)

	delta = (min_left < min_right ? min_left : min_right)
	final = delta ^ 2

	median = 0.5 * (x_left[end, 1] + x_right[1, 1])
	x_start, x_end = median - delta, median + delta

	pts_scanned = 0
	for i in 1:size(y, 1)
		p_curr = y[i, :]
		if x_start <= p_curr[1] <= x_end
			for j in i+1:size(y, 1)
				p_cmp = y[j, :]
				if p_cmp[2] - p_curr[2] >= delta
					break
				end
				if x_start <= p_cmp[1] <= x_end
					pts_scanned += 1
					dist = sqrdist(p_curr, p_cmp)	
					if dist < final
						final = dist
					end
				end
			end
		end
	end
	points_scanned[pts_scanned] = 1 + get(points_scanned, pts_scanned, 0)

	return sqrt(final)
end

function callclosestpair(arr)
    x = mergesort(arr,1)
    y = mergesort(arr,2)
    return closestpair(x,y)
end

function test()
	global points_scanned = Dict()
	for i in 1:1000
		pts = rand(10, 2) * 10
		dist = callclosestpair(pts)
	end
	println(points_scanned)
end
