from math import inf
from collections import deque

class Graph:
    """A graph stored in adjacency list representation"""
    def __init__(self, vertices, edges):
        self.vertices = {}
        for vertex in vertices:
            self.vertices[vertex] = []
        # *data represents additional data attached to the
        # edge. Append each neighbor and data to the
        # adjacency list for u.
        for u, v, *data in edges:
            self.vertices[u].append((v, *data))

    def adj(self, u):
        """Return neighbors of u"""
        return self.vertices[u]

    def V(self):
        """Return all vertices"""
        return self.vertices.keys()

    def E(self):
        """Return all edges"""
        return set(
            (u, v, *data) for u in self.V() for (v, *data) in self.adj(u)
        )

    def has_edge(self, u, v):
        """Check if there is an edge from u to v"""
        for vv, *_ in self.adj(u):
            if v == vv:
                return True
        return False

    def get_edge(self, u, v):
        """Return the data associated with edge (u, v)"""
        for vv, *data in self.adj(u):
            if v == vv:
                return data

    def set_edge(self, u, v, *data):
        """Set the data associated with edge (u, v)"""
        for i, (vv, *_) in enumerate(self.vertices[u]):
            if v == vv:
                self.vertices[u][i] = (v, *data)
                return

def rest_graph(G):
    """Return the residual network for G"""
    V = G.V()
    E = []
    for u, v, c, f in G.E():
        if f < c:
            E.append((u, v, c - f))
        if f > 0:
            E.append((v, u, f))
    return Graph(V, E)


def bfs(G, s, t):
    """Breadth-first-search in G from s to t"""

    # edge contains the edge we followed to get to v
    color = {}
    edge = {}
    for v in G.V():
        color[v] = 'white'
        edge[v] = None
    
    # Used as a FIFO queue
    Q = deque()

    color[s] = 'gray'
    Q.append(s)

    while len(Q) > 0:
        u = Q.popleft()
        color[u] = 'black'

        if u == t:
            break

        # Enqueue unvisited nodes
        for v, *data in G.adj(u):
            if color[v] == 'white':
                color[v] = 'gray'
                edge[v] = (u, data)
                Q.append(v)

    # We never found our target :(
    if edge[t] is None:
        return None

    # Construct the path s->t with edge data attached
    path = deque()
    node = t
    while node != s:
        pi, data = edge[node]
        path.appendleft((pi, node, *data))
        node = pi
    return list(path)

    
def edmons_karp(G, s, t):
    """Repeatedly finds an augmenting path
    and increases flow along it"""

    found_path = True
    while found_path:
        found_path = False

        rest = rest_graph(G)
        path = bfs(rest, s, t)

        if path is not None:

            # Find the highest allowed increase along this path
            increase = inf
            for u, v, f in path:
                increase = min(increase, f)
                found_path = True

            print(f'Found path {path} with increase {increase}')

            for u, v, _ in path:
                if G.has_edge(u, v):
                    # Increase flow on (u, v)
                    c, f = G.get_edge(u, v)
                    f += increase
                    G.set_edge(u, v, c, f)
                    print(f'Increased ({u}, {v}) by {increase} to {f}/{c}')

                else:
                    # Decrease flow on (u, v)
                    c, f = G.get_edge(v, u)
                    f -= increase
                    G.set_edge(u, v, c, f)
                    print(f'Decreased ({u}, {v}) by {increase} to {f}/{c}')

    print('Finished')

def get_flows(G, node, t):
    """Yield all flows from s to t"""
    if node == t:
        yield [t]
    else:
        # Loop through edges from node
        for v, _, f in G.adj(node):
            if f > 0:
                # Yield the edge from node to v with flow f
                # in addition to the path from v to t
                for flow in get_flows(G, v, t):
                    yield [node, f] + flow

def print_flows(G, s, t):
    """Prints every flow and the total flow of the network"""
    for flow in get_flows(G, s, t):
        print(flow)
    total_f = 0
    for (_, _, f) in G.adj(s):
        total_f += f
    print('Total flow:', total_f)

def case1():
    """Eksamen des. 2018, opg. 19, reversert rekkefølge"""
    V = ['s', 'A', 'B', 'C', 'D', 'A1', 'A2', 'A3', 'B1', 'B2', 'C1', 'C2', 'D1', '1', '2', '3', '4', 't']
    E = [
        ('s', 'A'),
        ('s', 'B'),
        ('s', 'C'),
        ('s', 'D'),

        ('A', 'A1'),
        ('A', 'A2'),
        ('A', 'A3'),

        ('B', 'B1'),
        ('B', 'B2'),

        ('C', 'C1'),
        ('C', 'C2'),

        ('D', 'D1'),

        ('A1', '1'),
        ('A2', '2'),
        ('A2', '3'),
        ('A3', '4'),
        
        ('B1', '2'),
        ('B2', '3'),
        ('B2', '4'),

        ('C1', '3'),
        ('C2', '4'),

        ('D1', '4'),

        ('1', 't'),
        ('2', 't'),
        ('3', 't'),
        ('4', 't')
    ]
    # Append c=1, f=0 to every edge
    for i, e in enumerate(E):
        E[i] = (*e, 1, 0)

    return V, E, 's', 't'

def case2():
    """Graf (a) på side 710(pdf 731) i boken"""
    V = [ 'Vancouver', 'Edmonton', 'Calgary', 'Saskatoon', 'Regina', 'Winnipeg']
    E = [
        ('Vancouver', 'Edmonton', 16),
        ('Vancouver', 'Calgary', 13),
        ('Edmonton', 'Saskatoon', 12),
        ('Calgary', 'Edmonton', 4),
        ('Calgary', 'Regina', 14),
        ('Saskatoon', 'Calgary', 9),
        ('Saskatoon', 'Winnipeg', 20),
        ('Regina', 'Saskatoon', 7),
        ('Regina', 'Winnipeg', 4)
    ]
    #Append f=0 to every edge
    for i, e in enumerate(E):
        E[i] = (*e, 0)
    return V, E, 'Vancouver', 'Winnipeg'

V, E, s, t = case2()

G = Graph(V, E)
edmons_karp(G, s, t)
print_flows(G, s, t)