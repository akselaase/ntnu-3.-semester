import numpy as np

class LogicPic:
    def __init__(self, pic):
        self._rows = pic['rows']
        self._cols = pic['cols']
        self._nrows = len(self._rows)
        self._ncols = len(self._cols)
        self._board = np.zeros((self._nrows, self._ncols), int)

    def step(self):
        self._try_empty()

    def _try_empty(self):
        for ri in self._empty_rows():
            rule = self._rows[ri]
            row = self._board[ri,:]
            n = 0
            for part in rule:
                row[n:n+part] = 1
                row[n + part] = -1
                n += part + 1
            empty = self._ncols - n + 1
            n = 0
            for part in rule:
                row[n:n+empty] = 0
                n += part + 1

    def _empty_rows(self):
        for ri in range(self._nrows):
            row = self._board[ri,:]
            if len(row.nonzero()) == 0:
                yield ri

    def __repr__(self):
        return str(self._board)

pic1 = {
    'rows': [
        (5,),
        (6,),
        (2,),
        (2,),
        (6,),
        (5,),
        (2,),
    ],
    'cols': [
        tuple(),
        (1,1),
        (2,2),
        (2,2),
        (7,),
        (7,),
        (2,2),
    ]
}