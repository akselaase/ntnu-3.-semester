from list import WrappableList, wrap_class

def _parent(i):
    return i // 2

def _left(i):
    return i * 2 

def _right(i):
    return i * 2 + 1


class Heap():
    def __init__(self, *args, **kwargs):
        list_type = kwargs.pop('list_type', list)
        self.list = list_type(*args, **kwargs)
        self._heapify_all()

    def _heapify_all(self):
        for i in range(len(self) // 2, 0, -1):
            self._heapify(i)

    def _heapify(self, i):
        size = len(self)
        largest = i
        l = _left(i)
        r = _right(i)
        if l <= size and self[l] > self[i]:
            largest = l
        if r <= size and self[r] > self[largest]:
            largest = r
        if largest != i:
            self[i], self[largest] = self[largest], self[i]
            self._heapify(largest)

    def __len__(self):
        return len(self.list)

    def __iter__(self):
        return iter(self.list)

    def __getitem__(self, index):
        return self.list[index - 1]

    def __setitem__(self, index, value):
        self.list[index - 1] = value
