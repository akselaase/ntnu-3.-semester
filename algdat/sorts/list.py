from collections.abc import Iterator
from wrap import wrap_class, count_calls

class WrappableList:

    def __iter__(self):
        return ListIterator(self)

class ListIterator(Iterator):

    def __init__(self, source):
        self._list = source
        self._iter = list.__iter__(source)

    def __iter__(self):
        self._list._inc_call_count('__iter__.__iter__')
        return self

    def __next__(self):
        self._list._inc_call_count('__iter__.__next__')
        return next(self._iter)

