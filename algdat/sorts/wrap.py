import types
from functools import partial

def _inc_call_count(stats, name):
    stats[name] = 1 + stats.get(name, 0)
    

def count_calls(func, stats=None):
    name = func.__name__

    if stats is not None:
        def wrapped_func(*args, **kwargs):
            print(f'called {name}')
            _inc_call_count(stats, name)
            return func(*args, **kwargs)
    else:
        def wrapped_func(self, *args, **kwargs):
            print(f'called {name}')
            _inc_call_count(self._stats, name)
            return func(self, *args, **kwargs)

    print(f'wrapped {name}')
    return wrapped_func


def wrap_class(wrapped, whitelist=None, blacklist=None, stats=None):

    def _wrap_special(func):
        name = func.__name__

        if blacklist and name in blacklist:
            print(f'{name} is blacklisted')
            return None
        
        if whitelist and name not in whitelist:
            print(f'{name} is not in whitelist')
            return None

        if not hasattr(wrapped, name):
            return None

        super_func = getattr(wrapped, name)
        return count_calls(super_func, stats)

    class Wrapped(wrapped):

        def __new__(cls, *args, **kwargs):
            obj = super().__new__(cls, *args, **kwargs)
            if stats is not None:
                obj._stats = stats
            else:
                obj._stats = {}
            return obj

        def _inc_call_count(self, name):
            _inc_call_count(self._stats, name)

        @_wrap_special
        def __init__(self, *args, **kwargs): pass
        @_wrap_special
        def __add__(self, value): pass
        @_wrap_special
        def __contains__(self, key): pass
        @_wrap_special
        def __delitem__(self, key): pass
        @_wrap_special
        def __dir__(self): pass
        @_wrap_special
        def __eq__(self, value): pass
        @_wrap_special
        def __format__(self, format_spec): pass
        @_wrap_special
        def __ge__(self, value): pass
        @_wrap_special
        def __gt__(self, value): pass
        @_wrap_special
        def __iadd__(self, value): pass
        @_wrap_special
        def __imul__(self, value): pass
        @_wrap_special
        def __iter__(self): pass
        @_wrap_special
        def __le__(self, value): pass
        @_wrap_special
        def __len__(self): pass
        @_wrap_special
        def __lenght_hint__(self): pass
        @_wrap_special
        def __next__(self): pass
        @_wrap_special
        def __lt__(self, value): pass
        @_wrap_special
        def __mul__(self, value): pass
        @_wrap_special
        def __ne__(self, value): pass
        @_wrap_special
        def __reduce__(self): pass
        @_wrap_special
        def __reduce_ex__(self, protocol): pass
        @_wrap_special
        def __repr__(self): pass
        @_wrap_special
        def __reversed__(self): pass
        @_wrap_special
        def __rmul__(self, value): pass
        @_wrap_special
        def __setitem__(self, key, value): pass
        @_wrap_special
        def __getitem__(self, key): pass
        @_wrap_special
        def __sizeof__(self): pass
        @_wrap_special
        def __str__(self): pass

    def _normal_func(name):
        if name[0:2] == '__':
            return False
        item = getattr(wrapped, name)
        return callable(item)

    if whitelist:
        names = whitelist
    else:
        names = dir(wrapped)

    for name in filter(_normal_func, names):
        func = getattr(Wrapped, name)
        wrapped_func = count_calls(func, stats)
        setattr(Wrapped, name, wrapped_func)

    return Wrapped

