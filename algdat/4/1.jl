## Du skal implementere denne funksjonen 
function countingsortletters(A,position)
	k = 30
	C = zeros(Int, k)
	B = Array{String}(undef, length(A))
	for i in 1:length(A)
		val = chartodigit(A[i][position])
		C[val] += 1
	end
	for i in 2:k
		C[i] += C[i - 1]
	end
	for i in length(A):-1:1
		val = chartodigit(A[i][position])
		pos = C[val]
		B[pos] = A[i]
		C[val] -= 1
	end
	return B
end

function countingsortlength(A)
	k = maximum(map(length, A)) + 1
	C = zeros(Int, k)
	B = Array{String}(undef, length(A))
	for i in 1:length(A)
		val = length(A[i]) + 1
		C[val] += 1
	end
	for i in 2:k
		C[i] += C[i - 1]
	end
	for i in length(A):-1:1
		val = length(A[i]) + 1
		pos = C[val]
		B[pos] = A[i]
		C[val] -= 1
	end
	return B
end

function flexradix(A, maxlength)
	A = countingsortlength(A)
	println(A)
	for i in length(A):-1:1
		biglen = length(A[i])
		shortlen = (i == 1 ? 0 : length(A[i-1]))
		if shortlen < biglen
			for position in biglen:-1:shortlen+1
				segment = A[i:end]
				res = countingsortletters(segment, position)
				A[i:end] = res
				println("Sort segment ", segment, " on position ", position, " -> ", res)
			end
		end
	end
	return A
end


# Don't mind me 
function chartodigit(character)
    #Dette er en hjelpefunksjon for å få omgjort en char til tall
    #Den konverterer 'a' til 1, 'b' til 2 osv.
    #Eksempel: chartodigit("hei"[2]) gir 5 siden 'e' er den femte bokstaven i alfabetet.
    return character - '`'
end

