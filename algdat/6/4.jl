
function backtrack(weights)
	rows, cols = size(weights)
	path = []
	
	cl = 1
	ch = cols
	for row in rows:-1:1
		col = cl + argmin(weights[row, cl:ch]) - 1
		push!(path, (row, col))

		cl = max(1, col-1)
		ch = min(cols, col+1)
	end

	return path
end
