function lislength(s)
    mls = zeros(Int, size(s))
    mls[1] = 1
    for i in 2:length(s)
        # Din kode her
        currmax = 1
		for j in 1:i-1
			if s[i] > s[j]
				currmax = max(currmax, mls[j] + 1)
	 		end
		end
		mls[i] = currmax
    end
    return maximum(mls) # Returnerer det største tallet i listen
end
