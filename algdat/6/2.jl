function smls(s)
    mls = zeros(Int, size(s))
    mls[1] = 1
    for i in 2:length(s)
        # Din kode her
        currmax = 1
		for j in 1:i-1
			if s[i] > s[j]
				currmax = max(currmax, mls[j] + 1)
	 		end
		end
		mls[i] = currmax
    end
	return mls
end

function lis(s, mls)
    ml = maximum(mls)
    lis = zeros(Int, ml)
    for i in length(mls):-1:1
		if mls[i] == ml
			lis[ml] = s[i]
			ml -= 1
		end
    end
    return lis
end

a = ([5, 3, 3, 6, 7])
b = ([2, 1, 4, 3, 6, 5])
