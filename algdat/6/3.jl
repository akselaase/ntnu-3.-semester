
function cumulative(weights)
	rows, cols = size(weights)
	pathweights = copy(weights)

	for r in 2:rows
		pathweights[r, 1] += minimum(pathweights[r-1, 1:2])
		pathweights[r, end] += minimum(pathweights[r-1, end-1:end])

		for c in 2:cols-1
			pathweights[r, c] += minimum(pathweights[r-1, c-1:c+1])
		end
	end

	return pathweights
end
