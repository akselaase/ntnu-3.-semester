using DataStructures: Queue, enqueue!, dequeue!

function bfs!(nodes, start)

	for node in nodes
		node.color = "white"
	end

	queue = Queue()
	enqueue!(queue, start)

	while length(queue) > 0
		node = dequeue!(queue)
		node.color = "black"
		if isgoalnode(node)
			return node
		end
		for neighbor in node.neighbors
			if neighbor.color == "white"
				neighbor.color = "gray"
				neighbor.predecessor = node
				enqueue!(queue, neighbor)
			end
		end
	end

	return nothing
end
