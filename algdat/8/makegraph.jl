
mutable struct Node
    i::Int
    j::Int
    floor::Bool
    neighbors::Array{Node}
    color::Union{String,Nothing}
    distance::Union{Int,Nothing}
    predecessor::Union{Node,Nothing}
end
Node(i, j, floor=true) = Node(i, j, floor, [], nothing, nothing, nothing)


function mazetonodelist(maze)

	nodes = Dict{Tuple{Int,Int}, Node}()
	one, two = size(maze)

	for i in 1:one
		for j in 1:two
			if maze[i, j] == 1
				node = Node(i, j)	
				nodes[(i, j)] = node
			end
		end
	end

	offsets = [ (-1, 0), (1, 0), (0, -1), (0, 1) ]
	for ((i, j), node) in nodes
		for (k, l) in offsets
			neighbor = get(nodes, (i + k, j + l), nothing)
			if neighbor != nothing
				push!(node.neighbors, neighbor)
			end
		end
	end

	return collect(values(nodes))
end

maze = [0 0 0 0 0 0 0
        1 1 0 1 1 1 0
        0 1 0 1 0 0 0
        0 1 0 1 1 1 0
        0 1 1 1 0 1 0
        0 1 0 1 0 1 1
        0 0 0 0 0 0 0]

list = mazetonodelist(maze)
println(list)
