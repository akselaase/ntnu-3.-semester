limit = 7

def A(n):
    if n - limit <= 1:
        target = 1
    else:
        target = (n // 7 - 1) * 7 + 2
    action = n - target
    while action < 1:
        action += limit
    while action > limit:
        action -= limit
    return action

def play(n):
    player = True
    while n > 0:
        action = A(n)
        n -= action
        player = not player
    return 'AAA' if player else 'b'

def predict(n):
    r = (n - 10) // 7 % 2
    return 'AAA' if r == 0 else 'b'

def sim(n):
    for i in range(1, n+1):
        print(i, predict(i), play(i))
