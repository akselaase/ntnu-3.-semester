
function mincoinsgreedy(coins, value)

	num = 0
	for coin in coins
		q, r = divrem(value, coin)
		num += q
		value = r
	end
	return num

end
