include("usegreed.jl")
include("greedy.jl")

function mincoins(coins, value)
	memo = Dict{Int, Int}()
	return mincoinsdynamic(coins, value, memo)
end

function mincoinsdynamic(coins, value, memo)
	if haskey(memo, value)
		println("Found ", value, ":", memo[value], " in memo")
		return memo[value]
	end
	if value in coins
		return 1
	end
	
	best = value
	for (index, coin) in enumerate(coins)
		if coin > value
			continue
		end
		sliced = coins[index:end]
		if usegreed(sliced)
			res = mincoinsgreedy(sliced, value)
			println("using greedy on ", value, " and ", sliced, " got ", res)
			if res < best
				best = res
			end
			return best
		else
			res = 1 + mincoinsdynamic(coins, value - coin, memo)
			println("using dynamic on ", value, ", ", coin, " and ", coins, " got ", res)
			if res < best
				best = res
			end
		end
	end
	memo[value] = best
	return best
end
