
class Node:
    def __init__(self, symbol, freq, left=None, right=None):
        self.symbol = symbol
        self.freq = freq
        self.left = left
        self.right = right

    def __lt__(self, other):
        return self.freq < other.freq

    def __add__(self, other):
        return Node(self.symbol + other.symbol,
                self.freq + other.freq,
                self,
                other)

    def __repr__(self):
        return f'{self.symbol}: {self.freq} ( {self.left} | {self.right} )'

nodes = [
    Node('a', 50),
    Node('b', 2),
    Node('c', 20),
    Node('d', 25),
    Node('e', 200),
    Node('f', 80),
    Node('g', 60)
]

def do():
    l = min(nodes)
    nodes.remove(l)
    r = min(nodes)
    nodes.remove(r)
    nodes.append(l + r)

while len(nodes) > 1:
    do()

root = nodes[0]

def printtree(tree, depth=0):
    if tree is not None:
        tree.bits = depth
        tree.weight = depth * tree.freq
        globals()[tree.symbol] = tree
        print(tree.symbol, depth)
        printtree(tree.left, depth+1)
        printtree(tree.right, depth+1)

printtree(root)
