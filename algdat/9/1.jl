mutable struct DisjointSetNode
    rank::Int
    p::DisjointSetNode
    DisjointSetNode() = (obj = new(0); obj.p = obj;)
end

function find_set(node)
    if node.p != node
        node.p = find_set(node.p)
    end
    return node.p
end

function union!(x, y)
    x = findset(x)
    y = findset(y)
    if x.rank > y.rank
        y.p = x
    else
        x.p = y
        if x.rank == y.rank
            y.rank = y.rank + 1
        end
    end
end