from doublelinkedlist import DoubleLinkedList

class ChainedHashTable:
    def __init__(self, size, Hash):
        self.T = [ DoubleLinkedList() for i in range(size) ]
        self.h = Hash(size)

    def insert(self, key, value):
        h = self.h(key)
        ll = self.T[h]
        ll.insert((key, value))

    def search(self, key):
        node = self.searchnode(key)
        return node.value
    
    def delete(self, key):
        node = self.searchnode(key)
        node.delete()

    def searchnode(self, key):
        h = self.h(key)
        ll = self.T[h]
        for node in ll.nodes():
            if node.value[0] == key:
                return node
        raise KeyError
