from .tree import TreeNode

class NaryTreeNode(TreeNode):
    def __init__(self, n, key, value=None):
        super().__init__(key, value)
        self._n = n
        self.children = [None] * n

    def add_node(self, node):
        for i in range(self._n):
            if self.children[i] is None:
                self.children[i] = node
                node.parent = self
                return i
        raise RuntimeError('Node full')

class BinaryTreeNode(NaryTreeNode):
    def __init__(self, key, value=None):
        super().__init__(2, key, value)

    @property
    def left(self):
        return self.children[0]

    @left.setter
    def _setleft(self, value):
        self.children[0] = value
    
    @property
    def right(self):
        return self.children[1]

    @right.setter
    def _setright(self, value):
        self.children[1] = value

class BinarySearchTreeNode(BinaryTreeNode):
    # TODO: node deletion / transplant
    def __init__(self, key, value):
        super().__init__(key, value)

    def search(self, key):
        if self.key == key:
            return self
        if key < self.key:
            if self.left is not None:
                return self.left.search(key)
        else:
            if self.right is not None:
                return self.right.search(key)

    def iterative_search(self, key):
        node = self
        while node is not None and node.key != key:
            if key < node.key:
                node = node.left
            else:
                node = node.right
        return node

    def insert(self, node):
        x = self.root
        while x is not None:
            y = x
            if node.key < x.key:
                x = x.left
            else:
                x = x.right
        node.parent = y
        if node.key < y.key:
            y.left = node
        else:
            y.right = node


    def min(self):
        node = self
        while node.left is not None:
            node = node.left
        return node

    def max(self):
        node = self
        while node.right is not None:
            node = node.right
        return node
    
    def successor(self):
        if self.right is not None:
            return self.right.min()
        else:
            x = self
            y = self.parent
            while y is not None and x == y.right:
                x = y
                y = y.parent
            return y
        
    def predecessor(self):
        if self.left is not None:
            return self.left.max()
        else:
            x = self
            y = self.parent
            while y is not None and x == y.left:
                x = y
                y = y.parent
            return y