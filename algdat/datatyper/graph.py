from doublelinkedlist import DoubleLinkedList

class Graph:
    def __init__(self, vertices, edges):
        self._V = vertices
        self._E = edges

    def V(self):
        return self._vertices()

    def E(self, source=None, target=None):
        seq = self._edges()
        if source is not None:
            seq = filter(lambda (u, v): u == source, seq)
        if target is not None:
            seq = filter(lambda (u, v): v == target, seq)
        return seq

    def _vertices(self):
        return self._V
    
    def _edges(self):
        return self._E

class AdjacencyListGraph(Graph):
    def __init__(self, vertices, edges):
        self._V = {
            vertex: DoubleLinkedList()
                for vertex in vertices
        }
        for u, v in edges:
            ll = self._V[u]
            ll.append(v)

    def _vertices(self):
        return self._V.keys()

    def E(self, source=None, target=None):
        if source is None:
            seq = itertools.chain.from_iterable(
                ll for ll in self._V.values()
            )
        else:
            seq = self._V[source]
        
        if target is not None:
            seq = filter(lambda (u, v): v == target, seq)
        
        return seq