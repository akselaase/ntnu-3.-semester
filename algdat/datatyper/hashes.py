from math import floor

class Hashes:
    
    @staticmethod
    def uniform_unit():
        def inner(size):
            def h(key):
                return floor(size * key)
            return h
        return inner

    @staticmethod
    def division():
        def inner(size):
            def h(key):
                return key % size
            return h 

    @staticmethod
    def multiplication(A=0.618):
        def inner(size):
            def h(key):
                return floor(size * (A * key % 1))
            return h
        return inner

    @staticmethod
    def perfecthash(sequence):
        def counter():
            i = 0
            while True:
                yield i
                i += 1
        
        hashes = {
            item: index for item, index in zip(sequence, counter())
        }
        def inner(size):
            def h(key):
                index = hashes[key]
                return index % size
            return h
        return inner

class DoubleHashes:

    @staticmethod
    def linear_probe(H1):
        def inner(size):
            h1 = H1(size)
            def h(key, i):
                return (h1(key) + i) % size
            return h
        return inner

    @staticmethod
    def quadratic_probe(H1, c1, c2):
        def inner(size):
            h1 = H1(size)
            def h(key, i):
                return (h1(key) + c1 * i + c2 * i ** 2) % size
            return h
        return inner

    @staticmethod
    def double_hash(H1, H2):
        def inner(size):
            h1 = H1(size)
            h2 = H2(size)
            def h(key, i):
                return (h1(key) + i * h2(key)) % size
            return h
        return inner