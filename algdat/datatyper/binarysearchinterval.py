from math import inf

class BinarySearchInterval:
    def __init__(self, initial=0, floats=False, growth=2):
        self.floats = floats
        self.growth = growth
        self.initial = initial
        self.value = initial
        self.min = -inf
        self.max = inf
        self.mindiff = 1
        self.maxdiff = 1

    def get(self):
        return self.value

    def remaining(self):
        return self.max - self.min

    def too_low(self):
        if self.min == self.max:
            return self.value
        self.min = self._getmin()
        if self.max == inf:
            self.maxdiff *= self.growth
            self.value = self.initial + self.maxdiff
        else:
            self.value = self._average()
        return self.value

    def too_high(self):
        if self.max == self.min:
            return self.value
        self.max = self._getmax()
        if self.min == -inf:
            self.mindiff *= self.growth
            self.value = self.initial - self.mindiff
        else:
            self.value = self._average()
        return self.value

    def nice(self):
        self.min = self.value
        self.max = self.value
    
    def _average(self):
        if self.floats:
            return (self.min + self.max) / 2
        return (self.min + self.max) // 2

    def _getmin(self):
        if self.floats:
            return self.value
        return min(self.value + 1, self.max)

    def _getmax(self):
        if self.floats:
            return self.value
        return max(self.value - 1, self.min)

    def __str__(self):
        return f'({self.min}, {self.value}, {self.max})'

    def __repr__(self):
        return f'({self.min}, {self.value}, {self.max}, {self.mindiff}, {self.maxdiff}, {self.floats})'


def test(n=1000):
    import random
    limit = 2**32

    for i in range(n):
        floats = random.choice([True, False])
        if floats:
            num = random.uniform(-limit, limit)
            start = random.uniform(-limit, limit)
        else:
            num = random.randrange(-limit, limit)
            start = random.randrange(-limit, limit)

        i = BinarySearchInterval(start, floats)

        while i.remaining() > 0:
            curr = i.get()
            if curr == num:
                i.nice()
            elif curr < num:
                i.too_low()
            else:
                i.too_high()
