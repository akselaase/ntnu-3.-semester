
class DoubleNode:
    def __init__(self, value):
        self.value = value
        self.prev = None
        self.next = None

    def connect(self, node):
        self.next = node
        node.prev = self

    def append(self, node):
        if self.next is not None:
            node.connect(self.next)
        self.connect(node)

    def insert(self, node):
        if self.prev is not None:
            self.prev.connect(node)
        node.connect(self)

    def delete(self):
        if self.prev is not None:
            self.prev.next = self.next
        if self.next is not None:
            self.next.prev = self.prev

class DoubleLinkedList:
    def __init__(self, sequence=None):
        self.head = DoubleNode(None)
        self.tail = DoubleNode(None)
        self.head.connect(self.tail)

        if sequence is not None:
            for val in sequence:
                self.append(val)

    def append(self, value):
        self.tail.insert(DoubleNode(value))

    def insert(self, value):
        self.head.append(DoubleNode(value))

    def __len__(self):
        l = 0
        for node in self:
            l += 1
        return l

    def nodes(self):
        return DoubleLinkedListIterator(self, nodes=True)

    def __iter__(self):
        return DoubleLinkedListIterator(self)

    def __reversed__(self):
        return DoubleLinkedListIterator(self, True)

class DoubleLinkedListIterator:
    def __init__(self, ll, reverse=False, nodes=False):
        self._ll = ll
        self._yield_nodes = nodes
        self._reverse = reverse
        if reverse:
            self._node = ll.tail
        else:
            self._node = ll.head

    def __iter__(self):
        return self
    
    def __next__(self):
        if self._reverse:
            self._node = self._node.prev
            if self._node == self._ll.head:
                raise StopIteration
        else:
            self._node = self._node.next
            if self._node == self._ll.tail:
                raise StopIteration
        
        if self._yield_nodes:
            return self._node
        else:
            return self._node.value