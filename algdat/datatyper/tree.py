
class TreeNode:
    def __init__(self, key, value=None):
        self.parent = None
        self.children = []
        self.root = None
        self.key = key
        self.value = value

    def add_child(self, node):
        self.children.append(node)
        if node is not None:
            node.parent = self
            node.root = self.root

    def set_root(self, root=None):
        if root is None:
            root = self
        self.root = root
        for child in self.children:
            child.set_root(root)