
class OpenHashTable:
    def __init__(self, size, DoubleHash):
        self._empty = (object(), )
        self._deleted = (object(), )
        self._size = size
        self.T = [self._empty] * size
        self.h = DoubleHash(size)

    def insert(self, key, value):
        i = 0
        while i < self._size:
            j = self.h(key, i)
            if self.T[j] in (self._empty, self._deleted):
                self.T[j] = (key, value)
                return j
            i += 1
        raise RuntimeError('Hashtable is full.')

    def search(self, key):
        j = self.searchindex(key)
        return self.T[j][1]

    def delete(self, key):
        j = self.searchindex(key)
        self.T[j] = self._deleted

    def searchindex(self, key):
        i = 0
        while i < self._size:
            j = self.h(key, i)
            val = self.T[j]
            if val == self._empty:
                break
            if val[0] == key:
                return j
            i += 1
        raise KeyError

    def items(self):
        for t in self.T:
            if t not in (self._empty, self._deleted):
                yield t