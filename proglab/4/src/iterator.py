
class Iterator:
    def __init__(self, iterable):
        self.iterator = iter(iterable)
        self.step()

    def is_empty(self):
        return not self.has_next

    def step(self):
        try:
            self.value = next(self.iterator)
            self.has_next = True
        except StopIteration:
            self.has_next = False

    def next(self):
        result = self.value
        self.step()
        return result
