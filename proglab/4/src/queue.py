from .container import Container


class Queue(Container):

    def peek(self):
        if self.is_empty():
            raise IndexError()
        return self._items[0]

    def pop(self):
        if self.is_empty():
            raise IndexError()
        return self._items.pop(0)
