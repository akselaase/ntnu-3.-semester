import string


class Tokenizer:

    def __init__(self, symbols):
        self.chars = set(string.ascii_letters + '~')
        self.digits = set(string.digits + '.')
        self.symbols = set(symbols)
        self.whitespace = set(string.whitespace)

    def process(self, text):
        token = ''

        for char in text:

            if char in self.symbols:
                if token:
                    yield self.parse_token(token)
                    token = ''

                yield char

            elif char in self.chars:
                if self.token_type(token) == 'number':
                    yield self.parse_token(token)
                    token = ''

                token += char

            elif char in self.digits :
                if self.token_type(token) == 'string':
                    yield self.parse_token(token)
                    token = ''

                token += char

            elif char in self.whitespace:
                if token:
                    yield self.parse_token(token)
                    token = ''

            else:
                raise ValueError(f'Invalid character "{char}"')

        if token:
            yield self.parse_token(token)

    def parse_token(self, token):
        token_type = self.token_type(token)

        if token_type == 'number':
            if '.' in token:
                return float(token)
            else:
                return int(token)

        elif token_type == 'string':
            if token != '':
                return token

        raise RuntimeError('Reached unreachable code')

    def token_type(self, token):
        if token == '':
            return 'none'

        if self.digits.intersection(token):
            return 'number'

        if self.chars.intersection(token):
            return 'string'

        else:
            raise ValueError(f'Invalid token "{token}"')
