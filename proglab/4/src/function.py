from inspect import signature


class Function:
    def __init__(self, func, precedence, debug=True):
        sig = signature(func)
        self.arguments = len(sig.parameters)
        self.name = func.__name__
        self.precedence = precedence
        self._func = func
        self._debug = debug

    def evaluate(self, *args, debug=None):
        result = self._func(*args)
        if debug or (debug is None and self._debug):
            strargs = map(str, args)
            print(f'{self.name}({", ".join(strargs)}) = {result}')
        return result

    def __call__(self, *args, debug=None):
        return self.evaluate(*args, debug=debug)

    def __repr__(self):
        return f'Function {self.name}, pr {self.precedence}'

    def __str__(self):
        return repr(self)
