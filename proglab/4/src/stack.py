from .container import Container


class Stack(Container):

    def peek(self):
        if self.is_empty():
            return None
        return self._items[-1]

    def pop(self):
        if self.is_empty():
            raise IndexError()
        return self._items.pop(-1)
