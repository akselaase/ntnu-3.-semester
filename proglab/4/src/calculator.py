import numbers

from .function import Function
from .rpnchain import RPNChain
from .stack import Stack
from .iterator import Iterator
from .tokenizer import Tokenizer


class Calculator:
    def __init__(self, functions, operators):
        self.functions = functions
        self.operators = operators

    def evaluate(self, text):
        chain = self.shunting_yard(text)
        return chain.evaluate()

    def shunting_yard(self, text):
        tokens = self.tokenify(text)

        chain = RPNChain()
        opstack = Stack()

        tokens = Iterator(tokens)

        while not tokens.is_empty():
            token = tokens.next()

            if isinstance(token, numbers.Number):
                chain.push(token)

            elif isinstance(token, Function):
                top = opstack.peek()
                while isinstance(top, Function) and \
                        top.precedence >= token.precedence:
                    chain.push(opstack.pop())
                    top = opstack.peek()
                opstack.push(token)

            elif token == '(':
                opstack.push(token)

            elif token == ')':
                while opstack.peek() != '(':
                    chain.push(opstack.pop())
                if opstack.is_empty():
                    raise ValueError('Mismatched parenthesis')
                else:
                    opstack.pop()

        while not opstack.is_empty():
            chain.push(opstack.pop())

        return chain

    def tokenify(self, text):
        symbols = ''.join(self.operators.keys()) + '()'
        tokenizer = Tokenizer(symbols)

        for token in tokenizer.process(text):
            if isinstance(token, numbers.Number):
                yield token
            elif token in '()':
                yield token
            elif token in symbols:
                yield self.operators[token]
            elif isinstance(token, str):
                yield self.lookup_name(token)
            else:
                raise RuntimeError(f'Got unexpected token "{token}"')

    def lookup_name(self, name):
        if name in self.functions:
            return self.functions[name]

        raise KeyError(f'Function "{name}" doesn\'t exist')
