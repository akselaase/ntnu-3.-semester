from .function import Function
from .queue import Queue
from .stack import Stack


class RPNChain:
    def __init__(self):
        self.tokens = Queue()

    def push(self, token):
        self.tokens.push(token)

    def evaluate(self):
        stack = Stack()

        while not self.tokens.is_empty():
            func = self.tokens.pop()

            if isinstance(func, Function):
                args = []
                if len(stack) < func.arguments:
                    raise ValueError(
                        f'Too few arguments to function {func.name}')
                while len(args) < func.arguments:
                    args.append(stack.pop())
                result = func.evaluate(*reversed(args))

            else:
                result = func

            stack.push(result)

        return stack
