
class Container:
    def __init__(self):
        self._items = []

    def is_empty(self):
        return len(self) == 0

    def push(self, item):
        self._items.append(item)

    def pop(self):
        raise NotImplementedError()

    def peek(self):
        raise NotImplementedError()

    def __len__(self):
        return len(self._items)

    def __repr__(self):
        return repr(self._items)

    def __str__(self):
        return str(self._items)
