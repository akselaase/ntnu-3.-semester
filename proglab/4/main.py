import sys
import math
import operator
from src.function import Function
from src.calculator import Calculator


modules = sys.argv[1:]
for mod in modules:
    module = getattr(__import__(f'src.{mod}'), mod)
    for name in dir(module):
        globals()[name] = getattr(module, name)


operators = {
        '+': Function(operator.add, 0),
        '-': Function(operator.sub, 0),
        '*': Function(operator.mul, 1),
        '/': Function(operator.truediv, 1),
        '^': Function(operator.pow, 2),
}

func_precedence = max(map(lambda func: func.precedence, operators.values())) + 1
functions = {
        'gcd': Function(math.gcd, func_precedence),
        'sin': Function(math.sin, func_precedence),
        'cos': Function(math.cos, func_precedence),
        'exp': Function(math.exp, func_precedence),
        'sqrt': Function(math.sqrt, func_precedence),
        '~': Function(operator.neg, func_precedence),
}

calc = Calculator(functions, operators)

for line in sys.stdin:
    print(calc.evaluate(line))
