import random
import numpy as np

from .cipher import Cipher, alphabet_size


class Caesar(Cipher):

    def _operate(self, data, key):
        return (np.array(data) + key) % alphabet_size

    @staticmethod
    def generate_keys():
        rng = random.Random()
        key = rng.randrange(1, alphabet_size)
        return (key, alphabet_size - key)

    @staticmethod
    def list_keys():
        return zip(
            range(1, alphabet_size),
            range(alphabet_size - 1, 0, -1)
        )
