from .person import Person


class Sender(Person):
    """Sender person"""

    def operate_cipher(self):
        """Operate cipher"""
        cleartext = input('Melding: ')
        return self.cipher.encrypt(cleartext)
