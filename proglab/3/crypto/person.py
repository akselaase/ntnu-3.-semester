
class Person:
    """A person"""

    def __init__(self):
        self.key = None
        self.cipher = None
        self.cipher_type = None

    def set_key(self, key):
        """Sets the key of the cipher"""
        self.key = key
        if self.cipher_type:
            self.cipher = self.cipher_type(key)

    def get_key(self):
        """Gets the cipher key"""
        return self.key

    def set_cipher(self, cipher):
        """Sets the cipher instance"""
        self.cipher = cipher
        self.cipher_type = type(cipher)

    def get_cipher(self):
        """Gets the cipher instance"""
        return self.cipher

    def set_cipher_type(self, cipher_type):
        """Sets the cipher type"""
        self.cipher_type = cipher_type
        if self.key:
            self.cipher = cipher_type(self.key)

    def get_cipher_type(self):
        """Gets the cipher type"""
        return self.cipher_type

    def operate_cipher(self):
        """Implementation specific"""
        raise NotImplementedError()
