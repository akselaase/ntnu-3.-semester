from collections import Counter
from string import ascii_letters
from .person import Person


def load_words(path='english_words.txt'):
    """Read all words"""
    text = open(path).read().splitlines()
    return set(text)

def clean_string(text):
    """Replace symbols with spaces"""
    text = text.lower()
    result = ''
    for char in text:
        if char in ascii_letters:
            result += char
        else:
            result += ' '
    return result


class Hacker(Person):
    """Hackerman"""

    def __init__(self):
        super().__init__()
        self.words = load_words()

    def hack(self, ciphertext):
        """Hack ciphertext"""
        candidates = Counter()
        for key in self.cipher_type.list_keys():
            self.set_key(key)
            decrypted = self.cipher.decrypt(ciphertext)
            score = self.evaluate_string(decrypted)
            candidates[key] = score
        self.set_key(candidates.most_common(1)[0][0])
        cleartext = self.cipher.decrypt(ciphertext)
        return candidates, cleartext

    def evaluate_string(self, text):
        """Evaluate a key"""
        clean = clean_string(text)
        parts = set(clean.split())
        if not parts:
            return 0
        intersection = parts.intersection(self.words)
        return sum(map(len, intersection)) + 0.1 * text.count(' ')

