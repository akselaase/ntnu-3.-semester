
ascii_start = 32
ascii_end = 126
alphabet_size = ascii_end - ascii_start + 1


def encode(text):
    return [(ord(char) - ascii_start) % alphabet_size for char in text]


def decode(data):
    return ''.join([chr(val % alphabet_size + ascii_start) for val in data])
