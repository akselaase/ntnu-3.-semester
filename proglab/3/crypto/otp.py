import numpy as np

from .cipher import Cipher, encode, alphabet_size


class OTP(Cipher):
    """One time pad cipher"""

    def __init__(self, key):
        if isinstance(key, str):
            key = np.array(encode(key))
            dkey = alphabet_size - key
            self.key = (key, dkey)
        else:
            self.key = key

    def _operate(self, data, key):
        """Implement OTP"""
        if len(key) < len(data):
            key = np.repeat(key, len(data) // len(key) + 1)
        key = key[:len(data)]
        return (data + key) % alphabet_size

    @staticmethod
    def generate_keys():
        """Generate random key"""
        key = np.random.randint(0, alphabet_size, 10)
        dkey = alphabet_size - key
        return (key, dkey)

    @staticmethod
    def list_keys():
        """List all keys"""
        return open('english_words.txt').read().splitlines()
