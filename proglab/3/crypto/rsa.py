import random

from .cipher import Cipher
from .crypto_utils import modular_inverse, generate_random_prime, blocks_from_text, text_from_blocks


class RSA(Cipher):

    def encrypt(self, data):
        """Encrypt data"""
        data = blocks_from_text(data, 4)
        res = [
            pow(value, self.key[0][1], self.key[0][0])
            for value in data
        ]
        return res

    def decrypt(self, data):
        """Decrypt data"""
        res = [
            pow(value, self.key[1][1], self.key[1][0])
            for value in data
        ]
        return text_from_blocks(res, 4)

    @staticmethod
    def generate_keys(bits=128):
        """Generate RSA keys"""
        p = generate_random_prime(bits)
        q = generate_random_prime(bits)

        n = p * q
        phi = (p - 1) * (q - 1)

        e = random.randint(3, phi - 1)
        d = modular_inverse(e, phi)
        while not d:
            e = random.randint(3, phi - 1)
            d = modular_inverse(e, phi)

        return ((n, e), (n, d))
