import random
import numpy as np

from .cipher import Cipher, alphabet_size
from .crypto_utils import modular_inverse


class Multiplicative(Cipher):

    @staticmethod
    def _operate(data, key):
        return (np.array(data) * key) % alphabet_size

    @staticmethod
    def generate_keys():
        r = random.Random()
        key = r.randrange(1, alphabet_size)
        modinv = modular_inverse(key, alphabet_size)
        while not modinv:
            key = r.randrange(1, alphabet_size)
            modinv = modular_inverse(key, alphabet_size)
        return (key, modinv)

    @staticmethod
    def list_keys():
        for key in range(2, alphabet_size):
            inv = modular_inverse(key, alphabet_size)
            if inv:
                yield (key, inv)
