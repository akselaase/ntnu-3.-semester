import random
import itertools
import numpy as np

from .cipher import Cipher, alphabet_size
from .caesar import Caesar
from .multiplicative import Multiplicative


class Affine(Cipher):

    def _encrypt(self, data):
        key1, key2 = self.key[0]
        return ((np.array(data) + key1) * key2) % alphabet_size

    def _decrypt(self, data):
        key1, key2 = self.key[1]
        return ((np.array(data) * key2) + key1) % alphabet_size

    @staticmethod
    def generate_keys():
        add_keys = Caesar.generate_keys()
        mul_keys = Multiplicative.generate_keys()
        return tuple(zip(add_keys, mul_keys))

    @staticmethod
    def list_keys():
        add_keys = Caesar.list_keys()
        mul_keys = Multiplicative.list_keys()
        for add, mul in itertools.product(add_keys, mul_keys):
            yield tuple(zip(add, mul))
