from .alphabet import encode, decode, alphabet_size


class Cipher:
    """Cipher superclass"""

    def __init__(self, key):
        self.key = key

    def verify(self, cleartext):
        """Verify that the cipher works"""
        ciphertext = self.encrypt(cleartext)
        return cleartext == self.decrypt(ciphertext)

    def encrypt(self, cleartext):
        """Encrypt data"""
        data = encode(cleartext)
        encrypted = self._encrypt(data)
        return decode(encrypted)

    def decrypt(self, ciphertext):
        """Decrypt data"""
        data = encode(ciphertext)
        decrypted = self._decrypt(data)
        return decode(decrypted)

    def _encrypt(self, data):
        """Overridable encryption routine"""
        return self._operate(data, self.key[0])

    def _decrypt(self, data):
        """Overridable decryption routine"""
        return self._operate(data, self.key[1])

    def _operate(self, data, key):
        """Operate function"""
        raise NotImplementedError()

    @staticmethod
    def generate_keys():
        """Generate keys"""
        raise NotImplementedError()

    @staticmethod
    def list_keys():
        """List all keys"""
        raise NotImplementedError()
