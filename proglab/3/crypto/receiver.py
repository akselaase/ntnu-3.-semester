from .person import Person


class Receiver(Person):
    """Receiver person"""

    def operate_cipher(self):
        """Operate cipher"""
        ciphertext = input('Kryptert tekst: ')
        return self.cipher.decrypt(ciphertext)
