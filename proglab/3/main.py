from crypto.otp import OTP as Cipher
from crypto.hacker import Hacker

text = 'hello everybody, how are you?'

key = 'pizza' #Cipher.generate_keys()
crypt = Cipher(key)

code = crypt.encrypt(text)
decode = crypt.decrypt(code)

hacker = Hacker()
hacker.set_cipher_type(Cipher)
candidates, decrypted = hacker.hack(code)

print('Actual key:', key)
print('Hacked key:', candidates.most_common(1)[0])
print('Actual text:', decode)
print('Hacked text:', decrypted)
