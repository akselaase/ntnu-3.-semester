import sys
import serial
from pysrc.morse_table import morse_table

class Mocoder:
    def __init__(self, source='stdin'):
        if source == 'stdin':
            self._read_one_signal = self._read_stdin
        else:
            try:
                self._read_one_signal = self._create_serial_reader(source)
            except:
                raise ValueError("source must be either 'stdin' or a serial port.")

        self.current_symbol = ''
        self.current_word = ''

    def _read_stdin(self):
        char = ' '
        try:
            while char.isspace():
                char = sys.stdin.read(1)
                if len(char) == 0:
                    self.handle_word_end()
                    sys.exit()
            return int(char)
        except ValueError:
            print('Invalid input', char, file=sys.stderr)

    def _create_serial_reader(self, port):
        ser = serial.Serial(
                port=port,
                baudrate=9600
        )

        def read():
            return int(ser.read(1))

        return read

    def step(self):
        signal = self.read_one_signal()
        self.process_signal(signal)

    def read_one_signal(self):
        return self._read_one_signal()

    def process_signal(self, signal):
        if signal in [0, 1]:
            self.update_current_symbol(str(signal))
        elif signal == 2:
            self.handle_symbol_end()
        elif signal == 3:
            self.handle_word_end()
        else:
            print('Invalid signal from arduino:', signal, file=sys.stderr)

    def update_current_symbol(self, signal):
        self.current_symbol += signal

    def handle_symbol_end(self):
        current_symbol = self.current_symbol
        if current_symbol == '':
            return
        try:
            symbol = morse_table[current_symbol]
            self.update_current_word(symbol)
        except:
            print('Invalid code', current_symbol, file=sys.stderr)
        finally:
            self.current_symbol = ''

    def update_current_word(self, symbol):
        self.current_word += symbol

    def handle_word_end(self):
        self.handle_symbol_end()
        print(self.current_word)
        self.current_word = ''


    
