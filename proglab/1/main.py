#!/usr/bin/env python3
import sys
from pysrc.mocoder import Mocoder

port = sys.argv[1]
decoder = Mocoder(port)

while True:
    decoder.step()

