const int SYM_NONE = -1;
const int SYM_DOT = 0;
const int SYM_DASH = 1;
const int SYM_SPAUSE = 2;
const int SYM_LPAUSE = 3;

const int TIME_BASE = 100;
const int TIME_DOT = TIME_BASE; 
const int TIME_DASH = 3 * TIME_BASE;
const int TIME_SPAUSE = 5 * TIME_BASE;
const int TIME_LPAUSE = 10 * TIME_BASE;

const int UNPRESSED = 0;
const int PRESSED = 1;

const int BUTTON_PIN = 2;
const int LED_DOT = 3;
const int LED_DASH = 4;

long lastButtonChange = 0;
long debounceDelay = 50;
int debounceState = LOW; //TODO: change default value

int symbolState = SYM_NONE;
int buttonState = UNPRESSED; 

void setup() {
	pinMode(BUTTON_PIN, INPUT);
	pinMode(LED_DOT, OUTPUT);
	pinMode(LED_DASH, OUTPUT);
	Serial.begin(9600);
}

void loop() {
	int currentSymbol = readSymbol();
	if (currentSymbol != SYM_NONE) {
		Serial.print(currentSymbol);
		if (currentSymbol == SYM_DOT) {
			digitalWrite(LED_DOT, HIGH);
			digitalWrite(LED_DASH, LOW);
		}
		else if (currentSymbol == SYM_DASH) {
			digitalWrite(LED_DOT, LOW);
			digitalWrite(LED_DASH, HIGH);
		}
		else {
			digitalWrite(LED_DOT, LOW);
			digitalWrite(LED_DASH, LOW);
		}
	}
}

/* OLD, NEW, STATE, TIME → SYM, NEWSTATE
 * u, u, !SPAUSE, [SPAUSE, LPAUSE] → SPAUSE
 * u, u, SPAUSE, [LPAUSE, →) → LPAUSE
 * p, u, *, [DOT, DASH) → DOT
 * p, p, !DASH, [DASH, →) → DASH
 */

int readSymbol() {
	long holdTime = millis() - lastButtonChange;
	int oldState = buttonState;
	int newState = readButton();
	buttonState = newState;

	int result = SYM_NONE;

	if (oldState == UNPRESSED && newState == UNPRESSED) {
		if (TIME_SPAUSE < holdTime && holdTime < TIME_LPAUSE && symbolState != SYM_SPAUSE) {
			result = SYM_SPAUSE;
			symbolState = SYM_SPAUSE;
		}
		else if (holdTime > TIME_LPAUSE && symbolState == SYM_SPAUSE) {
			result = SYM_LPAUSE;
			symbolState = SYM_LPAUSE;
		}
	}
	else if (oldState == PRESSED && newState == UNPRESSED) {
		if (TIME_DOT < holdTime && holdTime < TIME_DASH) {
			result = SYM_DOT;
		}
	}
	else if (oldState == PRESSED && newState == PRESSED) {
		if (holdTime > TIME_DASH && symbolState != SYM_DASH) {
			result = SYM_DASH;
			symbolState = SYM_DASH;
		}
	}

	if (oldState != newState) {
		symbolState = SYM_NONE;
	}

	return result;
}

int readButton() {
	if (millis() - lastButtonChange > debounceDelay) {
		int newState = digitalRead(BUTTON_PIN);
		if (newState != debounceState) {
			debounceState = newState;
			lastButtonChange = millis();
		}
	}

	if (debounceState == LOW)
		return UNPRESSED;
	else if (debounceState == HIGH)
		return PRESSED;
}
