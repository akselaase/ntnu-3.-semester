from collections import Counter
from src.enkelt_spill import EnkeltSpill


class MangeSpill:

    def __init__(self, player1, player2, games):
        self.player1 = player1
        self.player2 = player2
        self.num_games = games

    def arranger_enkeltspill(self):
        spill = EnkeltSpill(self.player1, self.player2)
        spill.gjennomfoer_spill()
        return spill

    def arranger_turnering(self):
        points = Counter()
        history = []

        for _ in range(self.num_games):
            spill = self.arranger_enkeltspill()
            history.append(spill.points)
            points.update(spill.points)

        return points, history
