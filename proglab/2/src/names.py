import random

NOUNS = ['Gamer', 'Kake', 'Sykkel', 'Fisk']
ADJECTIVES = ['Rar', 'Rik', 'Artig', 'Pen']


def noun():
    return random.choice(NOUNS)


def adjective():
    return random.choice(ADJECTIVES)
