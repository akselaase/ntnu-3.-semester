
class Action:
    def __init__(self, value):
        self.value = value

    def __neg__(self):
        opposites = {
            'stein': 'papir',
            'saks': 'stein',
            'papir': 'saks',
        }
        return Action(opposites[self.value])

    def __hash__(self):
        return hash(self.value)

    def __eq__(self, other):
        return self.value == other.value

    def __gt__(self, other):
        victories = [
            ('stein', 'saks'),
            ('saks', 'papir'),
            ('papir', 'stein')
        ]
        return (self.value, other.value) in victories

    def __lt__(self, other):
        return not (self == other or self > other)

    def __str__(self):
        return self.value


STEIN = Action('stein')
SAKS = Action('saks')
PAPIR = Action('papir')
ALLE = [STEIN, SAKS, PAPIR]
