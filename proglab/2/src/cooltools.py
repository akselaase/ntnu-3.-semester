
def sequence_equals(seq_a, seq_b):
    if len(seq_a) != len(seq_b):
        return False
    for a, b in zip(seq_a, seq_b):
        if a != b:
            return False
    return True


def find_subsequences(full, part):
    if not part:
        return
    try:
        offset = 0
        index = full.index(part[0])
        while index != -1:
            end = full[index:index+len(part)]
            if sequence_equals(end, part):
                yield index
            offset = index + 1
            index = full.index(part[0], offset)
    except ValueError:
        return
