import random
from collections import Counter

import src.names as names
from src.aksjon import ALLE
from src.players import Spiller
from src.cooltools import find_subsequences


class Historiker(Spiller):

    def __init__(self, husk=1):
        super().__init__(names.adjective() + 'Historiker')
        self.husk = husk
        self.actions = []

    def velg_aksjon(self):
        prev_actions = self.actions[-self.husk:]
        counts = Counter()
        for index in find_subsequences(self.actions[:-1], prev_actions):
            action = self.actions[index+self.husk]
            counts[action] += 1
        if counts:
            predicted_action = counts.most_common(1)[0][0]
        else:
            predicted_action = random.choice(ALLE)
        return -predicted_action

    def motta_resultat(self, mine, other, winner):
        self.actions.append(other)
