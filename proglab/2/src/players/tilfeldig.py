import random
import src.names as names
from src.aksjon import ALLE
from src.players import Spiller


class Tilfeldig(Spiller):

    def __init__(self):
        super().__init__('Tilfeldig' + names.noun())

    def velg_aksjon(self):
        return random.choice(ALLE)

    def motta_resultat(self, mine, other, winner):
        pass
