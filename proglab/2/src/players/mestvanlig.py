import random
from collections import Counter

import src.names as names
from src.aksjon import ALLE
from src.players import Spiller


class MestVanlig(Spiller):

    def __init__(self):
        super().__init__('MestVanlig' + names.noun())
        self.counts = Counter()

    def velg_aksjon(self):
        if self.counts:
            return -self.counts.most_common(1)[0][0]
        return random.choice(ALLE)

    def motta_resultat(self, mine, other, winner):
        self.counts[other] += 1
