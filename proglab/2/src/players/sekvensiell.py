import random
import src.names as names
from src.aksjon import ALLE
from src.players import Spiller


class Sekvensiell(Spiller):

    def __init__(self):
        super().__init__('Sekvensiell' + names.noun())
        order = list(ALLE)
        random.shuffle(order)

        def yeeter():
            while True:
                yield from order
        self.choice = yeeter()

    def velg_aksjon(self):
        return next(self.choice)

    def motta_resultat(self, mine, other, winner):
        pass
