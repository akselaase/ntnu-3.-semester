import src.names as names
from src.aksjon import ALLE
from src.players import Spiller


class Manuell(Spiller):

    def __init__(self):
        super().__init__('Manuell' + names.noun())

    def velg_aksjon(self):
        while True:
            user_input = input('Trekk: ').lower()
            for aksjon in ALLE:
                if aksjon.value.startswith(user_input):
                    return aksjon
            print('Invalid move.')

    def motta_resultat(self, mine, other, winner):
        pass
