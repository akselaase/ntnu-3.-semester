
class Spiller:
    def __init__(self, name='GenericSpiller'):
        self.name = name

    def velg_aksjon(self):
        raise NotImplementedError()

    def motta_resultat(self, mine, other, winner):
        raise NotImplementedError()

    def oppgi_navn(self):
        return self.name

    def __str__(self):
        return self.oppgi_navn()

    def __repr__(self):
        return self.oppgi_navn()
