from .spiller import Spiller
from .tilfeldig import Tilfeldig
from .sekvensiell import Sekvensiell
from .mestvanlig import MestVanlig
from .historiker import Historiker
from .manuell import Manuell
