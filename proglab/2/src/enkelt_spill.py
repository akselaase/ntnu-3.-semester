
class EnkeltSpill:
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2
        self.names = {
            self.player1: player1.oppgi_navn(),
            self.player2: player2.oppgi_navn()
        }
        self.aksjon1 = None
        self.aksjon2 = None
        self.points = None
        self.winner = None

    def gjennomfoer_spill(self):
        aksjon1 = self.player1.velg_aksjon()
        aksjon2 = self.player2.velg_aksjon()
        self.aksjon1, self.aksjon2 = aksjon1, aksjon2

        if aksjon1 == aksjon2:
            self.points = {self.player1: 0.5, self.player2: 0.5}
            self.winner = None
        if aksjon1 > aksjon2:
            self.points = {self.player1: 1, self.player2: 0}
            self.winner = self.player1
        if aksjon1 < aksjon2:
            self.points = {self.player1: 0, self.player2: 1}
            self.winner = self.player2

        self.player1.motta_resultat(aksjon1, aksjon2, self.winner)
        self.player2.motta_resultat(aksjon2, aksjon1, self.winner)

    def __str__(self):
        text = (f'{self.names[self.player1]} spilte {self.aksjon1}\n'
                f'{self.names[self.player2]} spilte {self.aksjon2}\n')
        if self.winner is not None:
            text += f'{self.names[self.winner]} vant!'
        else:
            text += 'Det ble uavgjort!'
        return text
