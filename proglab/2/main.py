#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

from src.enkelt_spill import EnkeltSpill
from src.mange_spill import MangeSpill
from src.players import Manuell, Tilfeldig, Sekvensiell, MestVanlig, Historiker

def select_player(prompt):
    player_type = input(prompt).lower()
    if 'tilfeldig'.startswith(player_type):
        return Tilfeldig()
    if 'sekvensiell'.startswith(player_type):
        return Sekvensiell()
    if 'mest vanlig'.startswith(player_type):
        return MestVanlig()
    if 'historiker'.startswith(player_type):
        husk = int(input('Husk: '))
        return Historiker(husk)

    print('Invalid player choice.')
    return None

def loop():
    player = None
    while not player:
        player = select_player('Select player [tilfeldig,sekvensiell,mest vanlig,historiker]: ')

    num = int(input('Antall spill: '))
    manuell = Manuell()
    for _ in range(num):
        spill = EnkeltSpill(manuell, player)
        spill.gjennomfoer_spill()
        print(spill)

def test(player1, player2, num_games):
    spill = MangeSpill(player1, player2, num_games)
    _, history = spill.arranger_turnering()
    points = list(map(lambda pts: pts[player1], history))
    raw = np.array(points)
    summed = np.cumsum(points) / np.arange(1, len(points) + 1)

    def plot(pts):
        plt.plot(pts, label=str(player1))
        plt.plot(1 - pts, label=str(player2))
        plt.legend(loc='upper right')
        plt.show()
    plot(raw)
    plot(summed)

try:
    while True:
        mode = input('Gamemode [spill selv/turnering]: ').lower()
        if 'spill selv'.startswith(mode):
            loop()
        elif 'turnering'.startswith(mode):
            player1 = select_player('Spiller 1 [tilfeldig,sekvensiell,mest vanlig,historiker]: ')
            player2 = select_player('Spiller 2 [tilfeldig,sekvensiell,mest vanlig,historiker]: ')
            num_games = int(input('Antall spill: '))
            test(player1, player2, num_games)
except EOFError:
    print()

